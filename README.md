# WayQt

The Qt-based library to handle Wayland and Wlroots protocols to be used with any Qt project.
Additionally, Wayfire's private protocol as well is supported. As the project develops, support
for custom protocols may be added.

This work is heavily inspired by the wrapland project. Instead of using qtwaylandscanner to
generate Qt classes for the wayland protocols, we use the raw C structs, as in the Wrapland
project. While this is a general purpose library, it is built to cater the needs of
[DesQ](https://desq.gitlab.io) and [PaperDE](https://cubocore.cc/paperde) projects.

If there is a feature that you need, please free to implement it and open a merge request.
You may also open an issue requesting for the feature.

### Dependencies:

* <tt>Qt5     (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>wayland (libwayland-dev, wayland-protocols)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>

### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/wayqt.git wayqt`
- Enter the `wayqt` folder
  * `cd wayqt`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### What works

* Regsitry - It's a light wrapper for wl_registry. Use this object to create
  WQt::LayerShell, WQt::XdgShell, and similar objects
* LayerShell - A wrapper around wlroots's layershell protocol. Once the layer
  shell object is created using the registry, you can use it to create LayerSurface
  objects.
* WindowManager - This is a wrapper over wlr_foreign_toplevel_manager interface. This
  class can be used to list out the windows. WindowHandle class handles actions
  related to individual windows, like minimize, maximize, etc.
* ToplevelManager - This is a wrapper over wayfire-toplevel-manager interface. This
  class can be used to list out the windows. Toplevel class handles actions
  related to individual windows, like minimize, maximize, etc. Additional functionalities
  such as toplevel's screenshots/preview, pin above others, etc. are available in this
  protocol.
* InputInhibitor - This helps a window grab all input from the compositor. To be used
  in lock-screens or keybinding's grabber etc.
* XdgShell - Wrapper over the xdg_shell C implementation. Can be used to create
  xdg_toplevel and xdg_popup objects.
* XdgTopLevel - Wrapper for the xdg_toplevel struct. This class can be used to control
  the toplevel views.
* ScreenCopy - Wrapper for the wlr_screencopy protocol. This class can be used to take
  screenshots of the whole screen.
* SessionLock - Wrapper for the ext_session_lock protocol. This class can be used to
  implement screenlocking.
* OutputManager - Wrapper for the wlr_output_management protocol.. This class is useful
  in controlling the outupt characteristics like resolution, scaling, rotation, etc.

### Known Bugs and Issues
* Popup menu's do not work with `WQt::LayerSurface`. <br>
  - **Workaround**: If you have a LayerSurface that wants to show a popup menu,
  create the menu, then set the menu's parent to `nullptr`.
  ```
  QMenu *menu = createMyMenu();
  menu->setParent( nullptr );
  menu->setWindowFlags( Qt::Popup | Qt::FramelessWindowHint );
  menu->popup();
  ```
  **Note**: This menu will be a toplevel view, and will follow the same rules
  as other toplevel views.

  - **Second Workaround** Compile and install examples/LayerShellIntegration.
  When running your application, set namespace to "desq-layer-shell" and the env
  variable QT_WAYLAND_SHELL_INTEGRATION=desq-layer-shell. This should allow menus
  to behave normally. This is available only with Qt6.

* examples/DataControl causes compositor crashes. Consider the Wlroots DataControl protocol
  implementation to be unstable.

### Upcoming
* Examples for various classes.
* Support for Wayland's newer protocols supported by wlroots 0.17
* Any other feature you request for... :)


### Special Mentions
* SendThread/ReceiveThread classes are heavily inspired by CopyQ.
