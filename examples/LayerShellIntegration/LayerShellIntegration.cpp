/**
 * The GPL v3 License
 *
 * Copyright (c) 2023 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2023 Victor Tran (https://github.com/vicr123)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "LayerShellIntegration.hpp"

#include <QtWaylandClient/private/qwaylanddisplay_p.h>
#include <QtWaylandClient/private/qwaylandshellintegrationfactory_p.h>
#include <QtWaylandClient/private/qwaylandwindow_p.h>

#include <wayqt/LayerShell.hpp>

#include "wlr-layer-shell-unstable-v1-client-protocol.h"
#include "xdg-shell-client-protocol.h"

WlrLayerShellIntegration::WlrLayerShellIntegration(): QtWaylandClient::QWaylandShellIntegration() {
}


bool WlrLayerShellIntegration::initialize( QtWaylandClient::QWaylandDisplay *display ) {
    display->addRegistryListener(
        [] (void *data, wl_registry *registry, quint32 name, const QString& interface, quint32 version) {
            WlrLayerShellIntegration *integration = static_cast<WlrLayerShellIntegration *>(data);

            if ( interface == "zwlr_layer_shell_v1" ) {
                integration->mLayerShell = new WQt::LayerShell(
                    (zwlr_layer_shell_v1 *)wl_registry_bind( registry, name, &zwlr_layer_shell_v1_interface, version ),
                    version
                );
            }
        },
        this
    );

    mXdgShell = QtWaylandClient::QWaylandShellIntegrationFactory::create( "xdg-shell", display );
    return mLayerShell != nullptr;
}


QtWaylandClient::QWaylandShellSurface * WlrLayerShellIntegration::createShellSurface( QtWaylandClient::QWaylandWindow *window ) {
    if ( (mLayerShell != nullptr) and shouldBeLayerShell( window ) ) {
        WQt::LayerSurface *surf = mLayerShell->getLayerSurface( window->window(), nullptr, WQt::LayerShell::Top, "desq-layer-shell" );
        return surf->getShellSurface();
    }

    auto              shellSurface = mXdgShell->createShellSurface( window );
    WQt::LayerSurface *lyrSurf     = WQt::LayerShell::forWindow( window->transientParent()->window() );

    if ( lyrSurf != nullptr ) {
        lyrSurf->getPopup( shellSurface->surfaceRole() );
        return shellSurface;
    }

    return nullptr;
}


bool WlrLayerShellIntegration::shouldBeLayerShell( QtWaylandClient::QWaylandWindow *window ) {
    auto windowType = window->window()->type();

    return !(windowType == Qt::Popup || windowType == Qt::ToolTip);
}
