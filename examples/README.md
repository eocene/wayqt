Following examples are included. Run all the examples inside a wlroots compositor, unless specified otherwise.

1. DataControl - A sample implementation of wlr-data-control protocol.
   For a complete implementation, look into DesQ Clipboard (https://gitlab.com/DesQ/DesQUtils/Clipboard.git)
1. FloatingButton - A sample implementation of layer shell/layer surface, along with Wayfire's interactive move (Wayfire only).
   For a complete implementation, look into DesQ Shell (https://gitlab.com/DesQ/Shell.git)
1. ScreenCopy - A sample implementation of wlr-screencopy-unstable protocol.
   For a complete implementation, look into DesQ Snaps (https://gitlab.com/DesQ/DesQUtils/Snaps.git)
