/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QObject>

struct wl_seat;

struct ext_idle_notifier_v1;
struct ext_idle_notification_v1;
struct ext_idle_notification_v1_listener;

namespace WQt {
    class IdleNotifier;
    class IdleNotification;
}


/**
 * WQt::IdleNotifier creates an instance of WQt::IdleNotification on a given seat,
 * with a given timeout.
 */
class WQt::IdleNotifier : public QObject {
    Q_OBJECT;

    public:
        IdleNotifier( ext_idle_notifier_v1 *idle );
        ~IdleNotifier();

        WQt::IdleNotification *createNotification( uint32_t, wl_seat * );

        ext_idle_notifier_v1 *get();

    private:
        /** Raw C pointer to this class */
        ext_idle_notifier_v1 *mObj;
};


/**
 * The WQt::IdleNotifier instance waits until the @timeout ms have elapsed
 * without user interaction, and emits a signal. It will then wait for user activity.
 */
class WQt::IdleNotification : public QObject {
    Q_OBJECT;

    public:
        IdleNotification( ext_idle_notification_v1 *timeout );
        ~IdleNotification();

        /** call this to begin receiving signals */
        void setup();

        /** Destroy this notification */
        void destroy();

        ext_idle_notification_v1 *get();

    private:
        static void handleIdled( void *, ext_idle_notification_v1 * );
        static void handleResumed( void *, ext_idle_notification_v1 * );

        /** Raw C pointer to this class */
        ext_idle_notification_v1 *mObj;

        /** Listener */
        static const ext_idle_notification_v1_listener mListener;

        bool mIsSetup = false;

        /** 0: idle; 1: activity */
        int pendingState = -1;

    Q_SIGNALS:
        /** Triggered when no user activity was registered in the requested idle time interval */
        void idled();

        /** Triggered on the first user activity after an idle event */
        void resumed();
};
