/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QObject>
#include <QHash>

struct wl_registry;
struct wl_display;
struct wl_seat;
struct wl_shm;
struct wl_output;
// struct wl_compositor;

struct wl_registry_listener;

struct xdg_wm_base;
struct org_kde_kwin_idle;
struct xdg_activation_v1;
struct zwlr_layer_shell_v1;
struct ext_idle_notifier_v1;
struct zwf_shell_manager_v2;
struct zdesq_shell_manager_v1;
struct zwlr_output_manager_v1;
struct zwlr_screencopy_manager_v1;
struct ext_session_lock_manager_v1;
struct zwlr_data_control_manager_v1;
struct zwlr_output_power_manager_v1;
struct zwayfire_toplevel_manager_v1;
struct zwlr_gamma_control_manager_v1;
struct zwlr_input_inhibit_manager_v1;
struct zwlr_foreign_toplevel_manager_v1;
struct zwp_keyboard_shortcuts_inhibit_manager_v1;

namespace WQt {
    class Registry;
    class XdgShell;

    class Output;
    class LayerShell;
    class IdleManager;
    class IdleNotifier;
    class OutputManager;
    class WindowManager;
    class XdgActivation;
    class ToplevelManager;
    class ScreenCopyManager;
    class DataControlManager;
    class SessionLockManager;
    class OutputPowerManager;
    class GammaControlManager;
    class InputInhibitManager;
    class ShortcutsInhibitManager;

    class Shell;
    class DesQShell;
}

class WQt::Registry : public QObject {
    Q_OBJECT;

    public:
        enum ErrorType {
            EmptyShm,
            EmptyIdle,
            EmptySeat,
            EmptyWlrIdle,
            EmptyDesQShell,
            EmptyXdgWmBase,
            EmptyCompositor,
            EmptyLayerShell,
            EmptyWayfireShell,
            EmptyOutputManager,
            EmptyWindowManager,
            EmptyXdgActivation,
            EmptyToplevelManager,
            EmptyScreenCopyManager,
            EmptyDataControlManager,
            EmptyOutputPowerManager,
            EmptySessionLockManager,
            EmptyShortcutsInhibitor,
            EmptyGammaControlManager,
            EmptyInputInhibitManager,
        };
        Q_ENUM( ErrorType );

        enum Interface {
            ShmInterface,
            IdleInterface,
            SeatInterface,
            WlrIdleInterface,
            DesQShellInterface,
            XdgWmBaseInterface,
            CompositorInterface,
            LayerShellInterface,
            WayfireShellInterface,
            OutputManagerInterface,
            WindowManagerInterface,
            XdgActivationInterface,
            ToplevelManagerInterface,
            ScreenCopyManagerInterface,
            DataControlManagerInterface,
            OutputPowerManagerInterface,
            SessionLockManagerInterface,
            ShortcutsInhibitorInterface,
            GammaControlManagerInterface,
            InputInhibitManagerInterface,
        };
        Q_ENUM( Interface );

        Registry( wl_display *wlDisplay );
        ~Registry();

        void setup();

        wl_registry *get();

        wl_display *waylandDisplay();
        wl_seat *waylandSeat();
        wl_shm *waylandShm();

        QList<WQt::Output *> waylandOutputs();

        /** List the already registered interfaces */
        QList<uint32_t> registeredInterfaces();

        /**
         * Wait upto @timeout milliseconds for interface @id to become available.
         * NOTE: This is a blocking call.
         */
        bool waitForInterface( WQt::Registry::Interface id, int timeout = 500 );

        /* Ready to use Wayland Classes */

        /**
         * XdgShell - Xdg Shell protocol implementation
         */
        WQt::XdgShell *xdgShell();

        /**
         * LayerShell - Wlr Layer Shell protocol implementation
         */
        WQt::LayerShell *layerShell();

        /**
         * WayfireShell - Wayfire Shell protocol implementation
         * DesQShell - DesQ Shell protocol implementation
         */
        WQt::Shell *wayfireShell();
        WQt::DesQShell *desqShell();

        /**
         * InputInhibitManager - Wlr Input Inhibitor protocol implementation
         */
        WQt::InputInhibitManager *inputInhibitManager();

        /**
         * WindowManager - Wlr TopLevel Manager protocol implementation
         */
        WQt::WindowManager *windowManager();

        /**
         * ToplevelManager - Wayfire TopLevel Manager protocol implementation
         */
        WQt::ToplevelManager *toplevelManager();

        /**
         * ScreenCopyManager - ScreenCopy Manager protocol implementation
         */
        WQt::ScreenCopyManager *screenCopier();

        /**
         * DataControlManager - DataControl protocol implementation
         */
        WQt::DataControlManager *dataControlManager();

        /**
         * OutputPowerManager - Output Power Management protocol implementation
         */
        WQt::OutputPowerManager *outputPowerManager();

        /**
         * OutputManager - Output Management protocol implementation
         */
        WQt::OutputManager *outputManager();

        /**
         * SessionLock - Ext Session Lock protocol implementation
         */
        WQt::SessionLockManager *sessionLockManager();

        /**
         * Gamma Control - Gamma Control Manager
         */
        WQt::GammaControlManager *gammaControlManager();

        /**
         * Idle - KDE's idle protocol implementation
         */
        WQt::IdleManager *idleManager();

        /**
         * Idle - Wayland's idle protocol implementation
         */
        WQt::IdleNotifier *idleNotifier();

        /**
         * XdgActivation
         */
        WQt::XdgActivation *xdgActivation();

        /**
         * ShortcutsInhibitor
         */
        WQt::ShortcutsInhibitManager *shortcutsInhibitor();

    private:
        /** Raw C pointer to this class */
        wl_registry *mObj = nullptr;

        /** wl_display object */
        wl_display *mWlDisplay = nullptr;

        /** wl_seat object */
        wl_seat *mWlSeat = nullptr;

        /** wl_shm object */
        wl_shm *mWlShm = nullptr;

        /** Connected outputs */
        QHash<uint32_t, WQt::Output *> mOutputs;

        /** List of registered interfaces */
        QList<uint32_t> mRegisteredInterfaces;

        /**
         * XdgShell objects;
         */
        xdg_wm_base *mXdgWmBase = nullptr;
        XdgShell *mXdgShell     = nullptr;

        /**
         * Layer Shell Objects
         */
        zwlr_layer_shell_v1 *mWlrLayerShell = nullptr;
        WQt::LayerShell *mLayerShell        = nullptr;

        /**
         * Wayfire and DesQ Shell Objects
         */
        zwf_shell_manager_v2 *mWfShellMgr     = nullptr;
        zdesq_shell_manager_v1 *mDesQShellMgr = nullptr;
        WQt::Shell *mWayfireShell             = nullptr;
        WQt::DesQShell *mDesQShell            = nullptr;

        /**
         * Wayfire Shell Objects
         */
        zwlr_input_inhibit_manager_v1 *mWlrInhibitMgr = nullptr;
        WQt::InputInhibitManager *mInhibitManager     = nullptr;

        /**
         * ForeignTopLevel Objects
         */
        zwlr_foreign_toplevel_manager_v1 *mWlrWindowMgr = nullptr;
        WQt::WindowManager *mWindowMgr = nullptr;

        /**
         * TopLevelManager Objects
         */
        zwayfire_toplevel_manager_v1 *mWfToplevelMgr = nullptr;
        WQt::ToplevelManager *mToplevelMgr           = nullptr;

        /**
         * ScreenCopyManager Objects
         */
        zwlr_screencopy_manager_v1 *mWlrScreenCopyMgr = nullptr;
        WQt::ScreenCopyManager *mScreenCopyMgr        = nullptr;

        /**
         * DataControlManager Objects
         */
        zwlr_data_control_manager_v1 *mWlrDataControlMgr = nullptr;
        WQt::DataControlManager *mDataControlMgr         = nullptr;

        /**
         * Output Power Manager Objects
         */
        zwlr_output_power_manager_v1 *mWlrOutputPowerMgr = nullptr;
        WQt::OutputPowerManager *mOutputPowerMgr         = nullptr;

        /**
         * Output Manager Objects
         */
        zwlr_output_manager_v1 *mWlrOutputMgr = nullptr;
        WQt::OutputManager *mOutputMgr        = nullptr;

        /**
         * Idle Objects - KWin
         */
        org_kde_kwin_idle *mKdeIdle   = nullptr;
        WQt::IdleManager *mWlrIdleMgr = nullptr;

        /**
         * Idle Objects - Wayland
         */
        ext_idle_notifier_v1 *mIdle = nullptr;
        WQt::IdleNotifier *mIdleMgr = nullptr;

        /**
         * Gamma Control Objects
         */
        zwlr_gamma_control_manager_v1 *mWlrGammaCtrl = nullptr;
        WQt::GammaControlManager *mGammaCtrl         = nullptr;

        /**
         * Session Lock Objects
         */
        ext_session_lock_manager_v1 *mExtSessLockMgr = nullptr;
        WQt::SessionLockManager *mSessLockMgr        = nullptr;

        /**
         * Xdg Activation Objects
         */
        xdg_activation_v1 *mWlXdgActivation = nullptr;
        WQt::XdgActivation *mXdgActivation  = nullptr;

        /**
         * Shortcuts Inhibitor Objects
         */
        zwp_keyboard_shortcuts_inhibit_manager_v1 *mWlShortcutsInhibitor = nullptr;
        WQt::ShortcutsInhibitManager *mShortcutsInhibitor = nullptr;

        static const wl_registry_listener mRegListener;
        static void globalAnnounce( void *data, wl_registry *registry, uint32_t name, const char *interface, uint32_t version );
        static void globalRemove( void *data, wl_registry *registry, uint32_t name );

        void handleAnnounce( uint32_t name, const char *interface, uint32_t version );
        void handleRemove( uint32_t name );

        QList<WQt::Registry::ErrorType> pendingErrors;
        QList<WQt::Output *> pendingOutputs;
        QList<WQt::Registry::Interface> pendingInterfaces;

        /** Flag to ensure setup() is called only once. */
        bool mIsSetup = false;

        /** emit errorOccured or store it in pending */
        void emitError( ErrorType );

        /**
         * emit output added/removed or store in pending.
         * bool indicates the state: true => added, false => removed.
         */
        void emitOutput( WQt::Output *, bool );

        /**
         * emit iInterface registered/deregistered, or store in pending.
         * bool indicates the state: true => registered, false => deregistered.
         */
        void emitInterface( WQt::Registry::Interface, bool );

    signals:
        void errorOccured( ErrorType et );

        void outputAdded( WQt::Output * );
        void outputRemoved( WQt::Output * );

        void interfaceRegistered( WQt::Registry::Interface );
        void interfaceDeregistered( WQt::Registry::Interface );
};
