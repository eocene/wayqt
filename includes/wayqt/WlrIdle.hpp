/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QObject>

struct wl_seat;

struct org_kde_kwin_idle;
struct org_kde_kwin_idle_timeout;
struct org_kde_kwin_idle_timeout_listener;

namespace WQt {
    class IdleManager;
    class IdleWatcher;
}


/**
 * WQt::IdleManager creates an instance of WQt::IdleWatcher on a given seat,
 * with a given timeout.
 */
class WQt::IdleManager : public QObject {
    Q_OBJECT;

    public:
        IdleManager( org_kde_kwin_idle *idle );
        ~IdleManager();

        WQt::IdleWatcher *getIdleWatcher( wl_seat *, int );

        org_kde_kwin_idle *get();

    private:
        /** Raw C pointer to this class */
        org_kde_kwin_idle *mObj;
};


/**
 * The WQt::IdleWatcher instance waits until the @timeout ms have elapsed
 * without user interaction, and emits a signal. It will then wait for user activity
 */
class WQt::IdleWatcher : public QObject {
    Q_OBJECT;

    public:
        IdleWatcher( org_kde_kwin_idle_timeout *timeout );
        ~IdleWatcher();

        void simulateUserActivity();
        void suspendWatch();

        org_kde_kwin_idle_timeout *get();

    private:
        static void handleIdleTimeOut( void *, org_kde_kwin_idle_timeout * );
        static void handleUserActivity( void *, org_kde_kwin_idle_timeout * );

        /** Raw C pointer to this class */
        org_kde_kwin_idle_timeout *mObj;

        /** Listener */
        static const org_kde_kwin_idle_timeout_listener mListener;

    Q_SIGNALS:
        /** Triggered when no user activity was registered in the requested idle time interval */
        void timedOut();

        /** Triggered on the first user activity after an idle event */
        void activityResumed();
};
