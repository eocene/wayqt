/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QObject>
#include <QThread>
#include <QFile>

/**
 * Timed IO
 * Sometimes, GTK apps cause problems while reading the data
 * they provide. In such cases, we should try to read data for
 * upto 1s, and then, quit. This QObject-derived class does
 * exactly what's written above.
 * Currently, we read the data in 4 batches of 250ms wait.
 */
class PipeReader : public QThread {
    Q_OBJECT;

    public:
        PipeReader();

        /** Try to read the data from pipe */
        void readFromPipe( int fd );

        /** Get the data */
        QByteArray data() const;

    protected:
        void run() override;

    private:
        void readData();

        QByteArray mData;
        int mFD;
};


class PipeWriter : public QThread {
    public:
        PipeWriter();

        void writeData( int fd, const QByteArray& data );

    protected:
        void run() override;

    private:
        QByteArray mData;
        int mFD;
};
