/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QObject>
#include <wayland-client.h>

#include "wayqt/Idle.hpp"
#include "ext-idle-notify-v1-client-protocol.h"

WQt::IdleNotifier::IdleNotifier( ext_idle_notifier_v1 *idle ) {
    mObj = idle;
}


WQt::IdleNotifier::~IdleNotifier() {
    ext_idle_notifier_v1_destroy( mObj );
}


WQt::IdleNotification *WQt::IdleNotifier::createNotification( uint32_t timeout, wl_seat *seat ) {
    ext_idle_notification_v1 *idle_timeout = ext_idle_notifier_v1_get_idle_notification( mObj, timeout, seat );

    return new WQt::IdleNotification( idle_timeout );
}


ext_idle_notifier_v1 *WQt::IdleNotifier::get() {
    return mObj;
}


WQt::IdleNotification::IdleNotification( ext_idle_notification_v1 *timeout ) {
    mObj = timeout;

    ext_idle_notification_v1_add_listener( mObj, &mListener, this );
}


WQt::IdleNotification::~IdleNotification() {
    if ( mObj ) {
        ext_idle_notification_v1_destroy( mObj );
    }
}


void WQt::IdleNotification::setup() {
    if ( mIsSetup == false ) {
        mIsSetup = true;

        if ( pendingState == 0 ) {
            emit idled();
        }

        else if ( pendingState == 1 ) {
            emit resumed();
        }
    }
}


void WQt::IdleNotification::destroy() {
    ext_idle_notification_v1_destroy( mObj );

    mObj = nullptr;
}


ext_idle_notification_v1 *WQt::IdleNotification::get() {
    return mObj;
}


void WQt::IdleNotification::handleIdled( void *data, ext_idle_notification_v1 * ) {
    IdleNotification *timer = reinterpret_cast<IdleNotification *>(data);

    if ( timer->mIsSetup ) {
        emit timer->idled();
    }

    else {
        timer->pendingState = 0;
    }
}


void WQt::IdleNotification::handleResumed( void *data, ext_idle_notification_v1 * ) {
    IdleNotification *timer = reinterpret_cast<IdleNotification *>(data);

    if ( timer->mIsSetup ) {
        emit timer->resumed();
    }

    else {
        timer->pendingState = 1;
    }
}


const struct ext_idle_notification_v1_listener WQt::IdleNotification::mListener = {
    handleIdled,
    handleResumed
};
