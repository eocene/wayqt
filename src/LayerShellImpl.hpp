/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QDebug>
#include <QWindow>

#include <wayland-client.h>

/** PRIVATE QT HEADERS: THESE CAN CHANGE AT ANY TIME!! */
#include <private/qwaylandsurface_p.h>
#include <private/qwaylandwindow_p.h>
#include <private/qwaylandshellsurface_p.h>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/XdgPopup.hpp>

#include "wlr-layer-shell-unstable-v1-client-protocol.h"

class LayerSurfaceImpl : public QtWaylandClient::QWaylandShellSurface {
    Q_OBJECT;

    public:
        LayerSurfaceImpl( QtWaylandClient::QWaylandWindow *window, zwlr_layer_surface_v1 *lyrSurf, uint version );
        ~LayerSurfaceImpl();

        void apply();

        void setSurfaceSize( const QSize& surfaceSize );
        void setAnchors( const WQt::LayerSurface::SurfaceAnchors& anchors );
        void setExclusiveZone( int exclusiveZone );
        void setMargins( const QMargins& margins );
        void setKeyboardInteractivity( WQt::LayerSurface::FocusType focusType );
        void setLayer( WQt::LayerShell::LayerType type );

        void getPopup( xdg_popup *popup );

        zwlr_layer_surface_v1 *get();

        Q_SIGNAL void configureSize( QSize );
        Q_SIGNAL void closeWindow();

    private:
        uint mVersion;
        QtWaylandClient::QWaylandWindow *mWindow;
        zwlr_layer_surface_v1 *mObj;

        QSize mSurfaceSize{ 0, 0 };
        int mExclusiveZone = -1;
        QMargins mMargins{ 0, 0, 0, 0 };
        uint mKeyboardInteractivity = 1;

        WQt::LayerSurface::SurfaceAnchors mAnchors;
        WQt::LayerShell::LayerType mLyrType;

        void configureSurface( uint32_t serial, uint32_t width, uint32_t height );
        void closeSurface();

        static void configureCallback( void *data, zwlr_layer_surface_v1 *object, uint32_t serial, uint32_t width, uint32_t height );
        static void closedCallback( void *data, zwlr_layer_surface_v1 *object );

        static const zwlr_layer_surface_v1_listener mLyrSurfListener;
};
