/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QObject>
#include <QDebug>
#include <QImage>
#include <wayland-client.h>
#include <wayqt/WayQtUtils.hpp>

#include "wayqt/OutputPowerManager.hpp"
#include "wlr-output-power-management-unstable-v1-client-protocol.h"

WQt::OutputPowerManager::OutputPowerManager( zwlr_output_power_manager_v1 *opMgr ) {
    mObj = opMgr;
}


WQt::OutputPowerManager::~OutputPowerManager() {
    zwlr_output_power_manager_v1_destroy( mObj );
}


WQt::OutputPower * WQt::OutputPowerManager::getOutputPower( wl_output *output ) {
    zwlr_output_power_v1 *opPwr = zwlr_output_power_manager_v1_get_output_power( mObj, output );
    OutputPower          *pwr   = new OutputPower( opPwr );

    return pwr;
}


WQt::OutputPower::OutputPower( zwlr_output_power_v1 *opMgr ) {
    mObj = opMgr;
    zwlr_output_power_v1_add_listener( mObj, &mListener, this );
}


WQt::OutputPower::~OutputPower() {
    zwlr_output_power_v1_destroy( mObj );
}

bool WQt::OutputPower::isEnabled() {
    if (mMode==1)
        return true;
    else 
        return false;
}

void WQt::OutputPower::setEnabled(bool enabled) {
    if(enabled)
        zwlr_output_power_v1_set_mode( mObj,1);
    else
        zwlr_output_power_v1_set_mode( mObj,0);    
    wl_display_flush( WQt::Wayland::display() );
}

void WQt::OutputPower::handleModeChange( void *data, struct zwlr_output_power_v1 *, uint32_t mode ) {
    WQt::OutputPower *pwr = reinterpret_cast<WQt::OutputPower *>(data);

    pwr->mMode = mode;
    emit pwr->modeChanged( mode );
}


void WQt::OutputPower::handleFailed( void *data, struct zwlr_output_power_v1 * ) {
    WQt::OutputPower *pwr = reinterpret_cast<WQt::OutputPower *>(data);
    emit pwr->failed();
}


const zwlr_output_power_v1_listener WQt::OutputPower::mListener = {
    handleModeChange,
    handleFailed
};
