/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QObject>
#include <wayland-client.h>

#include "wayqt/WlrIdle.hpp"
#include "idle-client-protocol.h"

WQt::IdleManager::IdleManager( org_kde_kwin_idle *idle ) {
    mObj = idle;
}


WQt::IdleManager::~IdleManager() {
    org_kde_kwin_idle_destroy( mObj );
}


WQt::IdleWatcher *WQt::IdleManager::getIdleWatcher( wl_seat *seat, int timeout ) {
    org_kde_kwin_idle_timeout *idle_timeout = org_kde_kwin_idle_get_idle_timeout( mObj, seat, timeout );

    return new WQt::IdleWatcher( idle_timeout );
}


org_kde_kwin_idle *WQt::IdleManager::get() {
    return mObj;
}


WQt::IdleWatcher::IdleWatcher( org_kde_kwin_idle_timeout *timeout ) {
    mObj = timeout;

    org_kde_kwin_idle_timeout_add_listener( mObj, &mListener, this );
}


WQt::IdleWatcher::~IdleWatcher() {
    if ( mObj ) {
        org_kde_kwin_idle_timeout_release( mObj );
    }
}


void WQt::IdleWatcher::simulateUserActivity() {
    org_kde_kwin_idle_timeout_simulate_user_activity( mObj );
}


void WQt::IdleWatcher::suspendWatch() {
    org_kde_kwin_idle_timeout_release( mObj );

    mObj = nullptr;
}


org_kde_kwin_idle_timeout *WQt::IdleWatcher::get() {
    return mObj;
}


void WQt::IdleWatcher::handleIdleTimeOut( void *data, org_kde_kwin_idle_timeout * ) {
    IdleWatcher *timer = reinterpret_cast<IdleWatcher *>(data);
    emit timer->timedOut();
}


void WQt::IdleWatcher::handleUserActivity( void *data, org_kde_kwin_idle_timeout * ) {
    IdleWatcher *timer = reinterpret_cast<IdleWatcher *>(data);
    emit timer->activityResumed();
}


const struct org_kde_kwin_idle_timeout_listener WQt::IdleWatcher::mListener = {
    handleIdleTimeOut,
    handleUserActivity
};
