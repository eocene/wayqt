/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QGuiApplication>
#include <QWindow>
#include <QScreen>
#include <QDebug>

#include <qpa/qplatformnativeinterface.h>
#include <wayland-client.h>
#include "xdg-shell-client-protocol.h"

#include "wayqt/XdgPopup.hpp"

WQt::XdgPopup::XdgPopup( xdg_surface *surface, xdg_popup *popup ) {
    mObj     = popup;
    mSurfObj = surface;

    xdg_surface_add_listener( mSurfObj, &mSurfListener, this );
    xdg_popup_add_listener( mObj, &mListener, this );
}


WQt::XdgPopup::~XdgPopup() {
    xdg_popup_destroy( mObj );
    xdg_surface_destroy( mSurfObj );
}


void WQt::XdgPopup::grab( wl_seat *seat, quint32 serial ) {
    xdg_popup_grab( mObj, seat, serial );
}


void WQt::XdgPopup::ackConfigure( quint32 serial ) {
    xdg_surface_ack_configure( mSurfObj, serial );
}


xdg_popup *WQt::XdgPopup::get() {
    return mObj;
}


xdg_surface *WQt::XdgPopup::xdgSurface() {
    return mSurfObj;
}


void WQt::XdgPopup::handleConfigure( void *data, xdg_popup *popup, int32_t x, int32_t y, int32_t w, int32_t h ) {
    Q_UNUSED( popup );

    XdgPopup *pop = reinterpret_cast<XdgPopup *>(data);

    pop->pendingRect = QRect( x, y, w, h );
}


void WQt::XdgPopup::handlePopupDone( void *data, xdg_popup *popup ) {
    Q_UNUSED( popup );
    XdgPopup *pop = reinterpret_cast<XdgPopup *>(data);
    emit pop->popupDone();
}


void WQt::XdgPopup::handlePopupRepositioned( void *data, xdg_popup *popup, uint32_t ) {
    Q_UNUSED( popup );
    XdgPopup *pop = reinterpret_cast<XdgPopup *>(data);
    emit pop->popupRepositioned();
}


void WQt::XdgPopup::handleSurfaceConfigure( void *data, xdg_surface *surf, uint32_t serial ) {
    Q_UNUSED( surf );

    XdgPopup *pop = reinterpret_cast<XdgPopup *>(data);
    emit pop->configureRequested( pop->pendingRect, serial );

    pop->pendingRect = QRect();
}


const xdg_popup_listener WQt::XdgPopup::mListener = {
    handleConfigure,
    handlePopupDone,
    handlePopupRepositioned
};

const xdg_surface_listener WQt::XdgPopup::mSurfListener = {
    handleSurfaceConfigure
};
