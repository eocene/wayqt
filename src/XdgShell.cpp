/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QtCore>

#include <qpa/qplatformnativeinterface.h>
#include <wayland-client.h>
#include "xdg-shell-client-protocol.h"

#include "wayqt/XdgShell.hpp"
#include "wayqt/XdgPositioner.hpp"
#include "wayqt/XdgTopLevel.hpp"
#include "wayqt/XdgPopup.hpp"

#define wDebug()    qDebug() << "[WLRQT] Create "

WQt::XdgShell::XdgShell( xdg_wm_base *base ) {
    mObj = base;
    xdg_wm_base_add_listener( mObj, &mListener, this );
}


WQt::XdgShell::~XdgShell() {
    xdg_wm_base_destroy( mObj );
}


WQt::XdgPositioner *WQt::XdgShell::createPositioner() {
    xdg_positioner *pos    = xdg_wm_base_create_positioner( mObj );
    XdgPositioner  *xdgPos = new XdgPositioner( pos );

    return xdgPos;
}


WQt::XdgTopLevel *WQt::XdgShell::createTopLevel( wl_surface *wlsurf ) {
    /** Create the xdg_surface object */
    xdg_surface *xsurf = xdg_wm_base_get_xdg_surface( mObj, wlsurf );

    if ( not xsurf ) {
        return nullptr;
    }

    /** Create the xdg_toplevel object */
    xdg_toplevel *toplevel = xdg_surface_get_toplevel( xsurf );
    XdgTopLevel  *xdgTop   = new XdgTopLevel( xsurf, toplevel );

    return xdgTop;
}


WQt::XdgPopup *WQt::XdgShell::createPopup( wl_surface *popSurf, xdg_surface *parSurf, xdg_positioner *placer ) {
    /** Create the xdg_surface object for popup */
    wDebug() << "Creating xdg_surface for popup wl_surface";
    xdg_surface *xpopsurf = xdg_wm_base_get_xdg_surface( mObj, popSurf );

    if ( not xpopsurf ) {
        return nullptr;
    }

    /** Create the xdg_popup object */
    wDebug() << "Creating xdg_popup from popup xdg_surface";
    xdg_popup *popup = xdg_surface_get_popup( xpopsurf, parSurf, placer );

    wDebug() << "Obtain XdgPopup from xdg_popup";
    XdgPopup *xPopup = new XdgPopup( xpopsurf, popup );

    wDebug() << "Return XdgPopup instance";
    return xPopup;
}


xdg_wm_base *WQt::XdgShell::get() {
    return mObj;
}


void WQt::XdgShell::handlePing( void *data, struct xdg_wm_base *shell, uint32_t serial ) {
    Q_UNUSED( data );
    xdg_wm_base_pong( shell, serial );
}


const xdg_wm_base_listener WQt::XdgShell::mListener = {
    handlePing
};
